#include <iostream>
#include <cstdlib>
#include <ctime>

int main() {

	// Selection inputs

	int menuSelection;
	char gameSelection;

	// Values for gameplay

	int yearsOfExperience;
	int codeSkill;
	int designSkill;
	int lives = 3;

	// Random Number Generation Ranges

	int codeSkillMax = 10;
	int codeSkillMin = 0;
	int designSkillMax = 10;
	int designSkillMin = 0;
	int yearsOfExperienceMax = 4;
	int yearsOfExperienceMin = 0;

	// Random Number Generators

	std::srand(std::time(0));
	int codeRNG = std::rand() % codeSkillMax + codeSkillMin;

	std::srand(std::time(0));
	int designRNG = std::rand() % designSkillMax + designSkillMin;

	std::srand(std::time(0));
	int experienceRNG = std::rand() % yearsOfExperienceMax + yearsOfExperienceMin;

	while (menuSelection != 2) {

		std::cout << "\n\nWelcome to cHire!\n";
		std::cout << "How to play: You are a member of the recruitment team in a software development company named Luey and must determine if you should hire the applier or not.";
		std::cout << "\nAppliers must have a coding skill of 5 or higher, and a design skill of 3 or higher. They must also have at least 2 years of experience.";
		std::cout << "\nEnter 'y' to hire and 'n' to decline. Enter 'e' to exit. If you get it wrong you lose a life. When you lose all of them, you get fired. You have 3 lives.";
		std::cout << "\n\n\n1. Start";
		std::cout << "\n2. Exit\n\n";
		std::cout << "Select whether to play or exit: ";

		std::cin >> menuSelection;

		if (menuSelection == 1) {

			std::cout << "\n\n\nGame Started";

			while (gameSelection != 'e', lives != 0) {
			
				std::cout << "\n\nLives: " << lives;
				std::cout << "\n\nApplier Experience: " << experienceRNG << " years";
				std::cout << "\nCoding Skill: " << codeRNG;
				std::cout << "\nDesign Skill:" << designRNG;
				std::cout << "Hire?: ";
				std::cin >> gameSelection;
				if (gameSelection == 'y', experienceRNG >= 2, codeRNG >= 5, designRNG >= 3) {

					std::cout << "\n\nGreat!\n\n";

				}
				else if (gameSelection == 'y', experienceRNG < 2, codeRNG < 5, designRNG < 3) {

					std::cout << "\n\nNot quite..\n\n";
					lives = lives - 1;
					if (lives == 0) {

						std::cout << "Sorry! You have been fired! You can play again or exit now.";

					}

				}
				else if (gameSelection == 'n', experienceRNG >= 2, codeRNG >= 5, designRNG >= 3) {

					std::cout << "\n\nNot quite..\n\n";
					lives = lives - 1;
					if (lives == 0) {

						std::cout << "Sorry! You have been fired! You can play again or exit now.";

					}

				}
				else if (gameSelection == 'e') {

					std::cout << "\n\nExiting now..";

				}
				else {
				
					std::cout << "\n\n\nCharacter/characters that were enter are an option for input";
				
				}
			
			}

		}
		else if (menuSelection == 2) {

			std::cout << "\n\n\nExiting...";

		}
		else {
		
			std::cout << "\n\n\nCharacter/characters that were enter are not on the list.";
		
		}

		std::cout << "\n\n\n----------------------------------------------------------------------------\n\n\n";

	}

}